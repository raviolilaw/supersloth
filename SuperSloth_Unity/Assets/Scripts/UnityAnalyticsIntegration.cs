﻿using UnityEngine;
using System.Collections;
using UnityEngine.Cloud.Analytics;


public class UnityAnalyticsIntegration : MonoBehaviour 
{
	
	// Use this for initialization
	void Start () 
	{
		
		const string projectId = "621e55b6-f1e6-4b63-bc46-ee2c6ef75a67";
		UnityAnalytics.StartSDK (projectId);
		
	}
	
}